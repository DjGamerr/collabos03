﻿Public Class Form1

    Public Function returnFizzBuzz(num As Integer) As String
        'first we define some local vars
        Dim isFizz = False
        Dim isBuzz = False
        Dim out As String = ""
        'then we check division
        If num Mod 3 = 0 Then isFizz = True
        If num Mod 5 = 0 Then isBuzz = True
        'if none are true then return number
        If isFizz = False And isBuzz = False Then
            out = num
        Else
            'otherwise return whatever is true
            If isFizz = True Then out += "Fizz"
            If isBuzz = True Then out += "Buzz"
        End If
        Return out
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ListBox1.Items.Clear()
        'lets check the input
        Dim rez As New List(Of String)
        If Not IsNumeric(TextBox1.Text) Then
            MsgBox(TextBox1.Text & " is not numeric!")
        Else
            If Val(TextBox1.Text) > 0 Then
                For i As Integer = 1 To Val(TextBox1.Text) Step 1
                    rez.Add(returnFizzBuzz(i))
                Next
                For Each item As String In rez
                    ListBox1.Items.Add(item)
                Next
            End If
        End If
    End Sub
End Class
